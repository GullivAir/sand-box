/* ------------------------------
 * Create_AmbassadAir_base.sql --
 * ------------------------------
 *
 * create empty database tables for AmbassadAir project
 * (to be used with compress.py and export.py)
 *
 * copyleft 2017 Vincent Mahé <vmahe@free.fr> for Gulliver
 * source under GNU General Public License v2
 * KooK, KooK.dev@free.fr: little changes to make command valid under sqlite3 database.
 */

CREATE TABLE IF NOT EXISTS station (
	id CHAR(32) NOT NULL,
	label CHAR(32) NOT NULL,
	password CHAR(10) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS measure (
	station_id CHAR(32) NOT NULL,
	value_kind CHAR(4) NOT NULL,
	day DATE NOT NULL,
	hour TIME NOT NULL,
	latitude FLOAT(13.8) NOT NULL,
	longitude CHAR(13.8) NOT NULL,
	altitude CHAR(13.8),
	value FLOAT(7.4) NOT NULL,
	label CHAR(80) NOT NULL
	-- ~ , CONSTRAINT pk_vaue PRIMARY KEY (station_id, value_kind, day, hour)
);

CREATE TABLE IF NOT EXISTS value_kind (
	kind CHAR(4) NOT NULL,
	label CHAR(80),
	unit CHAR(32),
	PRIMARY KEY (kind)
);

INSERT INTO value_kind(kind, label, unit) VALUES
	('PM10', 'Particule Matter under 10 micrometers', 'Micrograms/m3'),
	('TEFA', 'Temperature', 'Farenheit'),
	('SNDB', 'Sound Level', 'Decibels (dB)'),
	('HYGR', 'Hygrometry', 'Percents (%)');

