var express = require('express');
var app = express();
var sqlite3 = require('sqlite3').verbose();

db_path = 'database/serveur.db3'
var db  = new sqlite3.Database( db_path );

function getCoords( a_out )
{
}

// get coords WS
app.get('/coords', function (req, res)
{
	a_out = []

	//-- function getCoords
	// get a number between 48 and 49
	lon = Math.random() + 48

	// get a number between -2 and -1
	lat = Math.random() - 2

	a_out.push( [ lon, lat ] )

	console.log( a_out )
	res.type( 'json' )
	res.send( a_out )
})

// get coords WS
app.get('/coordsSqlite/:limit?', function (req, res)
{
	console.log( req.params["limit"] )

	var requete = "SELECT longitude,latitude FROM measure"
	if( req.params["limit"] )
	{
		requete += " limit " + req.params["limit"];
	}
	requete += ";"
	console.log( requete )

	db.all( requete, function( err, a_rows )
	{
		var a_data = []

		if( err )
			console.log( err );
		else
		{
			for( i=0; i<a_rows.length; ++i )
			{
				h_row = a_rows[i]
				//~ console.log	( h_row );
				a_lon_lat = [ h_row["longitude"], h_row['latitude'] ]
				a_data.push( a_lon_lat )
			}
			console.log	( a_data );
		}

		res.type( 'json' )
		res.send( a_data )
	});
})


// launch server
var server = app.listen(8081, function ()
	{
		var host = server.address().address
		var port = server.address().port

		console.log("Example app listening at http://%s:%s", host, port )
	})
