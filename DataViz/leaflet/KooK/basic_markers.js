/*
 * L is defined by leaflet
 * data should be defined be user.
 *	we could imagine a get_markers_coords function to be called or web service.
*/

// add a marker to mymap
/*
var marker = L.marker([48.1,-1.6]).addTo(mymap);
var m1 = L.marker([48.1,-1.7]).addTo(mymap);
var m2 = L.marker([48.12,-1.6]).addTo(mymap);
var m3 = L.marker([48.12,-1.7]).addTo(mymap);
*/
//data = JSON.parse( "[ [48.138,-1.663], [48.123,-1.71] ]" )
//~ data = [ [48.1,-1.6], [48.1,-1.7], [48.12,-1.6], [48.12,-1.7] ]
for( var _i=0; _i<data.length; ++_i )
{
	//~ alert( JSON.stringify( data[_i] ) );
	var m = L.marker( data[_i] ).addTo(mymap);
	m.bindPopup( JSON.stringify( data[_i] ) );
}


/*
// add a circle to mymap
var circle = L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500 // en mètre
}).addTo(mymap);

// add a polygon to mymap
var polygon = L.polygon( data ).addTo(mymap);
*/
